# Activity 16: Introduction to Behavior-Driven Development with Cucumber


## Content Learning Objectives
After completing this activity, students should be able to:

* Understand and explain the principles of Behavior-Driven Development (BDD).
* Write Gherkin scenarios to describe software behavior.
* Implement step definitions in Java using Cucumber.
* Run BDD tests and interpret the results.

## Process Skill Goals
During the activity, students should make progress toward:

* Interpreting user requirements and translating them into executable specifications. (Information Processing)
* Collaboratively writing test scenarios and implementing tests. (Teamwork and Communication)
* Analyzing the output of BDD tests to refine and correct software behavior. (Critical Thinking)

## Team Roles
* Manager: Coordinates the activity, ensures that all roles are filled and that deadlines are met.
* Presenter: Responsible for summarizing the team's findings and presenting them to the class.
* Recorder: Keeps detailed notes on decisions made during the activity, including any changes to the initial test scenarios.
* Reflector: Provides feedback on the team's process and suggests improvements.
  
## Model 1: Understanding BDD and Gherkin Syntax
Content:

Introduction to BDD principles: collaboration, documentation, and examples.
Overview of Gherkin syntax: Features, Scenarios, Given, When, Then.
Example: User login feature.

```
Feature: User Login
  Scenario: Successful login with correct credentials
    Given the user has navigated to the login page
    When the user inputs a valid username and password
    Then the user should be redirected to the dashboard

  Scenario: Unsuccessful login with wrong credentials
    Given the user has navigated to the login page
    When the user inputs an incorrect username or password
    Then the user should see an error message
```
Questions:

What is the purpose of the Given, When, Then structure?

- The "Given, When, Then" structure in Gherkin syntax is used to define the context, actions, and expected outcomes for a scenario in Behavior-Driven Development (BDD). This format helps to clearly articulate:

- Given: The preconditions or initial state before the main action is taken. This sets the stage for the scenario.
- When: The specific action or event that occurs. This is the trigger or the main operation being tested.
- Then: The expected result or state after the action. This describes the changes or effects expected from the "When" step.
- This structure ensures scenarios are easy to understand and follow, aligning closely with natural language and business understanding.



How do scenarios in a feature help to clarify requirements?

- Scenarios in a feature provide a detailed, practical outline of how software should behave in specific situations. By defining concrete examples of user interactions and expected outcomes.

## Model 2: Implementing Tests with Cucumber

Content:

Setting up a Java project with Cucumber.

Writing step definitions for Gherkin scenarios.

Running Cucumber tests and understanding the results.

#Example: Implementation of the step definitions for the login feature.

```
import org.junit.Assert;
import io.cucumber.java.en.*;

public class LoginSteps {
    private String username;
    private String password;
    private boolean loginSuccess;

    @Given("the user has navigated to the login page")
    public void navigateToLoginPage() {
        // Code to simulate navigation
    }

    @When("the user inputs a valid username and password")
    public void inputValidCredentials() {
        username = "validUser";
        password = "validPass";
        loginSuccess = true;  // Simulate successful login
    }

    @When("the user inputs an incorrect username or password")
    public void inputInvalidCredentials() {
        username = "invalidUser";
        password = "invalidPass";
        loginSuccess = false;  // Simulate failed login
    }

    @Then("the user should be redirected to the dashboard")
    public void redirectToDashboard() {
        Assert.assertTrue("User is not redirected to dashboard", loginSuccess);
    }

    @Then("the user should see an error message")
    public void showError() {
        Assert.assertFalse("Error message not shown", loginSuccess);
    }
}
```
Questions:

How do step definitions link with Gherkin scenarios?

- Step definitions in Cucumber are the Java methods that implement the executable code for the steps defined in Gherkin scenarios. Each step in a Gherkin scenario (Given, When, Then) is linked to a step definition through matching patterns in the step text. This linkage allows Cucumber to:

- Recognize the text of each step in the scenario.
- Execute the corresponding Java method when the step is run.
- Validate the behavior of the application against the expected outcomes defined in the scenario.

What changes would you make to the test if the login process added a CAPTCHA?

- Adding a new step in the scenarios to handle CAPTCHA validation, such as "And the user solves the CAPTCHA."
- Updating the step definitions to simulate or bypass the CAPTCHA validation during testing, depending on the test environment setup (e.g., integrating with CAPTCHA solving services or using a test key that bypasses CAPTCHA in a test environment).

## Model 3: Advanced Scenario Writing

Content:

Writing complex Gherkin scenarios (with Background, Scenario Outline, Examples).
Handling parameterized tests.
Example: Testing different user roles.


```
Feature: Role-Based Dashboard Access
  Background: User is logged in
    Given a user of type {string} is logged in successfully

  Scenario Outline: Accessing features based on role
    When the user navigates to the <feature>
    Then the user <access> access the feature

  Examples:
    | role       | feature      | access  |
    | Admin      | Admin Panel  | can     |
    | Subscriber | Admin Panel  | cannot  |
```
Questions:

What is the advantage of using Scenario Outline with Examples?
- Using a Scenario Outline with Examples in Gherkin allows for parameterization of scenarios. This means:

- You can run the same scenario multiple times with different data sets (specified in the Examples table), which is efficient for testing various inputs and outcomes without rewriting scenarios.It ensures broader test coverage and helps in identifying edge cases by varying parameters systematically.

How does the Background keyword improve scenario readability and reusability?
- The Background keyword in Gherkin is used to define steps that are common to all scenarios in a feature file. This improves:

- Readability: By removing repetitive steps from each scenario, making each scenario shorter and clearer.
- Reusability: By consolidating common setup steps into a single section that is automatically applied to every scenario, reducing duplication and maintenance efforts.

### Steps for Practical Implementation (Remainder of class time)

Technician: Fork this repository to your team's subgroup.

Team members: Collaborate on writing additional scenarios and implementing corresponding step definitions.

Complete and review: Push changes to the repository, review test results, and make adjustments as necessary.


By the end of this activity, students should have a comprehensive understanding of BDD, its practical application, and the collaborative nature of writing executable specifications that double as documentation.