package test.java.edu.worcester.cs.step_definitions;

import test.java.edu.worcester.cs.Customer;
import io.cucumber.java.en.*;
import static org.junit.Assert.*;

public class CustomerSteps {
    private Customer customer;
    private int discount;

    @Given("a customer has {int} past orders")
    public void a_customer_has_past_orders(Integer pastOrders) {
        customer = new Customer(pastOrders);
    }

    @When("the customer makes a purchase of ${double}")
    public void the_customer_makes_a_purchase_of(Double orderTotal) {
        discount = customer.calculateOrderDiscount(orderTotal);
    }

    @Then("the discount should be {int} percent")
    public void the_discount_should_be_percent(Integer expectedDiscount) {
        assertEquals(expectedDiscount, (Integer) discount);
    }
}
